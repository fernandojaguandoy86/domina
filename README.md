# domina

El comprimido del desarrollo de la prueba consta de 3 carpetas y un archivo sql  

<b>Front</b>: Interfaces de aplicación (frontend) que conectan con los microservicios.
<br>-> Para arrancar: en la carpeta <b>Front</b> del proyecto, se ejecuta: <b>ng serve</b>

<b>Tareas</b>: microservicio (Bakend) que gestiona las tareas 
<br>-> Para arrancar: en la carpeta <b>Tareas</b> del proyecto, se ejecuta: <b>npm run dev</b>

<b>Login</b>: microservicio (Backend) que gestiona los usuarios
<br>-> Para arrancar: en la carpeta <b>Login</b> del proyecto, se ejecuta: <b>npm run dev</b>

<b>domina2.sql</b>: script de la base de datos en postgresql del proyecto 

NOTA: Para hacer la conexión de los microservicios con la base de datos, las credenciales se encuentran en el archivo <b>.env</b> de las carpetas <b>Login</b> y <b>Tareas</b>


## Getting started



## Add your files
